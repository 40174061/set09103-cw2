Armchairfans

This app allows users to create chat rooms based around sporting events so that users can disucss the event

The app cannot run in levinux as it requires node.js to work. It has been agreed that this app will not be required to wok within levinux.

This app requires gevent and socketio which can be installed using the following commands

pip install gevent
pip install flask-socketio

Run the app from the src folder with:
python armchairfans.py

There is a known issue with counting users using safari, firefox and IE as the function to leave the room is not called in these browsers. For the meantime, this app works best in chrome.