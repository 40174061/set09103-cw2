function isEmail(email) {
  var regex = /^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$/;
  return regex.test(email);
}

$(function(){

    $("#sendbutton").click(function(){
      	socket.send($("#myMessage").val());
       	$("#myMessage").val("");
    })

    $(".full-height-nav").height($(window).height()-$(".navbar").height());
    $(".full-height").height($(window).height());

    $("#findMore").click(function(e){
        e.preventDefault();
        $('html, body').animate({
            scrollTop: $("#findOutMore").offset().top
        }, 1000);
    });
    $("#backTop").click(function(e){
        e.preventDefault();
        $("html body").animate({
            scrollTop: 0
        }, 1000)
    })

    $("#registration").submit(function(e){
        var email = $("#email").val();
        var password = $("#password").val();
        var cPassword = $("#cPassword").val();
        if (!isEmail(email)) {
            e.preventDefault();
            $("#email").val("");
            $("#password").val("");
            $("#cPassword").val("");
            alert(email+" is not a valid email address");
        } else if (password !== cPassword) {
            e.preventDefault();
            $("#password").val("");
            $("#cPassword").val("");
            alert("Passwords do not match");
        } else {
            return;
        }        
    });

    if ($(".error").length > 0) {
        alert($(".error").html());
    }

    $(".create-event").click(function(){
        $(this).hide();
        $(".new-event").show();
    });
    $("#cancelCreate").click(function(){
        $(".new-event").hide();
        $(".create-event").show();
    });

    $(".room-link").click(function(){
        window.location = $(this).data("href");
    });

    $("#showRoomDetails").click(function(){
        if ($(this).html() == "Show Details") {
            $(this).html("Hide Details");
            $(".room-chat").height($(window).height()-$(".roomDetails").height()-300);
        } else {
            $(this).html("Show Details");
            $(".room-chat").height($(window).height()-300);
        }
        $(".roomDetails").toggle();
    });

    $(".room-chat").height($(window).height()-300);
});