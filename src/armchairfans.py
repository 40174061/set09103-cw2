from flask import Flask, g, render_template, url_for, redirect, session, request, abort
from functools import wraps
from flask_socketio import SocketIO, emit, join_room, leave_room, close_room, rooms, disconnect
from datetime import datetime, timedelta
from logging.handlers import RotatingFileHandler
import ConfigParser, bcrypt, sqlite3, json, urllib, logging, random

async_mode = None

app = Flask(__name__)
db_location = 'var/armchair.db'
json_location = 'var/armchair.json'
app.config['SECRET_KEY'] = '\xde\xb5\xffER\xf5\x9c;\\\xe0\x9e\xf0\xa5&\xe7\xea\x8e\x0c\x99=\xee\x1d\xf2\xf0'
socketio = SocketIO(app, async_mode=async_mode)
thread = None

#unction for checking if user is logged in
def requires_login(f):
	@wraps(f)
	def decorated(*args, **kwargs):
		status = session.get('logged_in', False)
		if not status:
			return redirect(url_for('login'))
		return f(*args, **kwargs)
	return decorated

#get the db and open a connection
def get_db():
	db = getattr(g, 'db', None)
	if db is None:
		db = sqlite3.connect(db_location)
		g.db = db
	return db

#closes the database connection when the user leaves the page
@app.teardown_appcontext
def close_db_connection(exception):
	db = getattr(g, 'db', None)
	if db is not None:
		db.close()

#initiates the database and starts fresh. This should only be used when the app is initialised
def init_db():
	with app.app_context():
		db = get_db()
		with app.open_resource('schema.sql', mode='r')as f:
			db.cursor().executescript(f.read())
		db.commit()

#check if the username or email address are already registered
def checkUser(username, email):
	db = get_db()
	checkUsernameSQL = "SELECT username FROM users WHERE username = ?"
	for row in db.cursor().execute(checkUsernameSQL, [username]):
		return "Username already in use"
	checkEmailSQL = "SELECT email FROM users WHERE email = ?";
	for row in db.cursor().execute(checkEmailSQL, [email]):
		return "Email address already in use"
	return False

#check the user details when logging in
def check_auth(user, password):
	db = get_db()
	loginSQL = "SELECT id, password FROM users WHERE username = ?"
	isUser = False
	for row in db.cursor().execute(loginSQL, [user]):
		rID = row[0]
		rPassword = str(row[1])
		isUser = True
	if (isUser and
		rPassword == bcrypt.hashpw(password.encode('utf-8'), rPassword)):
		return rID
	return False

#checks the room exists
def checkRoom(title, sport):
	db = get_db()
	checkroomSQL = "SELECT id FROM rooms WHERE title = ? AND sport = ? AND created >= datetime('now', '-1 day')"
	for row in db.cursor().execute(checkroomSQL, (title, sport)):
		return True
	return False

#adds a user to the database ile showing how many users are in each room
def addUserToRoom(id):
	db = get_db()
	addUserSQL = "UPDATE rooms SET users = users + 1 WHERE id = ?"
	db.cursor().execute(addUserSQL, [id])
	db.commit()

#gets the number of users in the room
def getUsersInRoom(id):
	db = get_db()
	getUsersSQL = "SELECT users FROM rooms WHERE id = ?"
	for row in db.cursor().execute(getUsersSQL, [id]):
		users = row[0]
		return users
	return "Failed to find room"

#removes the user rom the room in the database
def leaveRoom(id):
	db = get_db()
	leaveRoomSQL = "UPDATE rooms SET users = users - 1 WHERE id = ?"
	db.cursor().execute(leaveRoomSQL, [id])
	db.commit()

#INITIATE LOGGING
def logs(app):
	log_pathname = app.config['log_location'] + app.config['log_file']
	file_handler = RotatingFileHandler(log_pathname, maxBytes=1024*1024*10, backupCount=1024)
	file_handler.setLevel(app.config['log_level'])
	formatter = logging.Formatter("%(levelname)s | %(asctime)s | %(module)s | %(funcName)s | %(message)s")
	file_handler.setFormatter(formatter)
	app.logger.setLevel(app.config['log_level'])
	app.logger.addHandler(file_handler)

def gettotalusers():
	db = get_db()
	totalusersSQL = "SELECT SUM(users) FROM rooms WHERE created >= datetime('now', '-1 day')"
	for row in db.cursor().execute(totalusersSQL):
		return row[0]
	return 0

def gettotalrooms():
	db = get_db()
	totalroomsSQL = "SELECT COUNT(*) FROM rooms WHERE created >= datetime('now', '-1 day')"
	for row in db.cursor().execute(totalroomsSQL):
		return row[0]
	return 0

#INITIATE THE APP
def init(app):
	config = ConfigParser.ConfigParser()
	try:
		config_location = "etc/config.cfg"
		config.read(config_location)

		app.config['DEBUG'] = config.get("config", "debug")
		app.config['ip_address'] = config.get("config", "ip_address")
		app.config['port'] = config.get("config", "port")
		app.config['url'] = config.get("config", "url")

		#logging configuration
		app.config['log_file'] = config.get("logging", "name")
		app.config['log_location'] = config.get("logging", "location")
		app.config['log_level'] = config.get("logging", "level")
	except:
		print"Could not read config from\: ", config_location

#route index page. if user is not logged in they are redirected to login page
@app.route("/", methods=['GET', 'POST'])
@requires_login
def root():
	#if the create event form has been submitted
	if request.method == 'POST':
		sport = request.form['sport']
		title = request.form['title']
		if checkRoom(title, sport):
			error = "A room named "+title+" already exists"
			return render_template('index.html', error=error), 200
		db = get_db()
		createroomSQL = "INSERT INTO rooms (sport, title) VALUES (?, ?)"
		db.cursor().execute(createroomSQL, (sport, title))
		db.commit()
		for row in db.cursor().execute("SELECT last_insert_rowid()"):
			id = str(row[0])
		return redirect("/sports/"+sport+"/"+title+"/"+id)#redirect user to their created event

	with open(json_location, 'r') as data_file:
		data = json.load(data_file)
		sports = data["sports"]

	totalusers = gettotalusers();
	totalrooms = gettotalrooms();
	db = get_db()
	rooms = {}
	i = 0
	topeventsSQL = "SELECT id, title, sport, users FROM rooms WHERE created >= datetime('now', '-1 day') ORDER BY users DESC LIMIT 4"
	for row in db.cursor().execute(topeventsSQL):
		rooms[i] = [row[0], row[1], row[2], row[3]]
		i += 1
	newrooms = {}
	newroomsSQL = "SELECT id, title, sport, users FROM rooms WHERE created  >= datetime('now', '-1 day') ORDER BY created DESC LIMIT 4"
	i = 0
	for row in db.cursor().execute(newroomsSQL):
		newrooms[i] = [row[0], row[1], row[2], row[3]]
		i += 1
	if len(rooms) > 0: #either rooms or newrooms can be used as both would return errors if no rooms have been created
		return render_template('index.html', sports=sports, rooms=rooms, newrooms=newrooms, totalusers=totalusers, totalrooms=totalrooms), 200
	error = "No events are running right now"
	return render_template('index.html', sports=sports, error=error), 200

@app.route("/myaccount/", methods=['GET', 'POST'])
@requires_login
def account():
	hasError = False
	username = session.get('username')
	db = get_db()
	if request.method == 'POST':
		formtype = request.form['formtype']

		if formtype == "email":
			email = str(request.form['email'])
			updatedetailsSQL = "UPDATE users SET email = ? WHERE username = ?"
			db.cursor().execute(updatedetailsSQL, (email, username))
			db.commit()

		elif formtype == "password":

			password = str(request.form['password'])

			if check_auth(username, password):
				npassword = str(request.form['nPassword'])
				passwordhash = bcrypt.hashpw(npassword, bcrypt.gensalt())
				updatepasswordSQL = "UPDATE users SET password = ? WHERE username = ?"
				db.cursor().execute(updatepasswordSQL, (passwordhash, username))
				db.commit()
				return redirect(url_for('logout'))

			else:
				try:
					session['failed_pass'] += 1
				except KeyError:
					session['failed_pass'] = 0
				if session['failed_pass'] > 2:
					failed = str(session['failed_pass'])
					app.logger.info(failed+" failed attempts to update password for "+username)
				error = "Incorrent password"
				hasError = True

	userdetailsSQL = "SELECT email FROM users WHERE username = ?"
	for row in db.cursor().execute(userdetailsSQL, [username]):
		email = row[0]
	if hasError:
		return render_template("myaccount.html", username=username, email=email, error=error), 200
	return render_template("myaccount.html", username=username, email=email), 200

@app.route("/sports/")
@requires_login
def sports():
	with open(json_location, 'r') as data_file:
		data = json.load(data_file)
		sports = data["sports"]

	db = get_db()
	roomCounts = {}
	roomcountSQL = "SELECT sport, COUNT(title) FROM rooms WHERE created >= datetime('now', '-1 day') GROUP BY sport"
	for row in db.cursor().execute(roomcountSQL):
			roomCounts[row[0]] = row[1]	

	return render_template("sports.html", sports=sports, roomCounts=roomCounts), 200

@app.route("/sports/<sport>/")
@requires_login
def sport(sport):
	with open(json_location, 'r') as data_file:
		data = json.load(data_file)
		sports = data["sports"]
	if sport not in sports:
		abort(404)
	db = get_db()
	rooms = {}
	i = 0
	checkroomsSQL = "SELECT title, id, users FROM rooms WHERE sport = ? AND created >= datetime('now', '-1 day') ORDER BY users DESC"
	for row in db.cursor().execute(checkroomsSQL, [sport]):
		rooms[i] = [row[0], row[1], row[2]]
		i += 1
	if len(rooms) > 0:
		return render_template("rooms.html", sport=sport, rooms=rooms), 200
	error = "No rooms have been created in the last 24 hours"
	return render_template("rooms.html", sport=sport, error=error), 200
	

@app.route("/sports/<sport>/<title>/<id>")
@requires_login
def room(sport, title, id):
	db = get_db()
	roomExists = False
	roomdetailsSQL = "SELECT strftime('%d/%m/%Y %H:%M', created) AS created, strftime('%d/%m/%Y %H:%M', datetime(created, '+1 day')) AS expires FROM rooms WHERE id = ? AND sport = ? AND title = ? AND created >= datetime('now', '-1 day')"
	for row in db.cursor().execute(roomdetailsSQL, (id, sport, title)):
		created = row[0]
		expires = row[1]
		roomExists = True
	username = session['username']
	if not checkRoom(title, sport) or roomExists == False:
		app.logger.info("Attempted access to access sports/"+sport+"/"+title+"/"+id)
		abort(404)
	return render_template("room.html", title=title, sport=sport, id=id, username=username, created=created, expires=expires), 200

@app.route("/login/", methods=['GET', 'POST'])
def login():
	if request.method == 'POST':
		user = request.form['username']
		pw = request.form['password']

		if check_auth(user, pw):
			session['logged_in'] = True
			session['username'] = user
			session['login_attempts'] = 0
			r = lambda: random.randint(0,150)
			col = ('#%02X%02X%02X' % (r(),r(),r()))
			session['mycolor'] = col
			return redirect(url_for('.root'))
		try:
			session['login_attempts'] += 1
		except KeyError:
			session['login_attempts'] = 1
		if session['login_attempts'] > 2:
			logins = str(session['login_attempts'])
			app.logger.info(logins+" failed login attempts with the username "+user)
		return render_template('login.html', error="Username or Password Incorrect"), 200
	return render_template('login.html'), 200

@app.route("/logout/")
def logout():
	session['logged_in'] = False
	session['username'] = None
	return redirect(url_for('login'))

@app.route("/register/", methods=['GET', 'POST'])
def register():
	if request.method == 'POST':
		username = str(request.form['username'])
		email = str(request.form['email'])
		password = str(request.form['password'])
		passwordhash = bcrypt.hashpw(password, bcrypt.gensalt())

		userExists = checkUser(username, email);
		if userExists is False:
			createUserSQL = "INSERT INTO users (id, username, email, password) VALUES (NULL, ?, ?, ?)"
			db = get_db()
			db.cursor().execute(createUserSQL, (username, email, passwordhash))
			db.commit()
			return redirect(url_for('login'))
		return userExists
	return render_template('register.html'), 200

@socketio.on('join', namespace='/test')
def join(message):
	roomid = message['room']
	join_room(roomid)
	addUserToRoom(roomid)
	users = getUsersInRoom(roomid)
	emit('updateusers',
		{'data': str(users)},
		room=roomid)
	emit('roomupdate', {'data': str(users), 'room': str(roomid)}, broadcast=True)
	emit('totalusers', {'data': str(gettotalusers())}, broadcast=True)
	emit('totalrooms', {'data': str(gettotalrooms())}, broadcast=True)

@socketio.on('leave', namespace='/test')
def leave(message):
	roomid = message['room']
	leave_room(roomid)
	leaveRoom(roomid)
	users = getUsersInRoom(roomid)
	emit('updateusers',
		{'data': str(users)},
		room=roomid)
	emit('joinleave')
	emit('roomupdate', {'data': str(users), 'room': str(roomid)}, broadcast=True)
	emit('totalusers', {'data': str(gettotalusers())}, broadcast=True)
	emit('totalrooms', {'data': str(gettotalrooms())}, broadcast=True)

@socketio.on('my_room_event', namespace='/test')
def send_room_message(message):
	emit('my_response',
		 {'data': message['data'], 'color': session.get('mycolor')},
		 room=message['room'])


@socketio.on('disconnect_request', namespace='/test')
def disconnect_request():
	emit('my_response',
		 {'data': 'Disconnected!'})
	disconnect()

@socketio.on('connect', namespace='/test')
def test_connect():
	emit('my_response', {'data': 'Connected'})

@app.errorhandler(404)
def page_not_found(error):
	return render_template('404.html'), 404

if __name__ == '__main__':
	init(app)
	logs(app)
	socketio.run(
		app, 
		host=app.config['ip_address'],
		port=int(app.config['port']))