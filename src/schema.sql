DROP TABLE if EXISTS users;
DROP TABLE if EXISTS rooms;

CREATE TABLE users (
	id INTEGER PRIMARY KEY,
	username text,
	email text,
	password text
);

CREATE TABLE rooms (
	id INTEGER PRIMARY KEY,
	sport text,
	title text,
	created date,
	users int
);